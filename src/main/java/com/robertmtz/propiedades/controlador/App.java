/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.robertmtz.propiedades.controlador;

import com.robertmtz.propiedades.controlador.impl.Csv;
import com.robertmtz.propiedades.modelo.SistemaOperativo;

/**
 *
 * @author UPTAP-Developer
 */
public class App {

    public static void main(String[] args) {
        Csv so = new Csv("datos-debian", new SistemaOperativo(3, "LINUX-DEBIAN"));
        so.exportar();
    }
}
