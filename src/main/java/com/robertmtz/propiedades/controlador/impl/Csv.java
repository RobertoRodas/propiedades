/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.robertmtz.propiedades.controlador.impl;

import com.csvreader.CsvWriter;
import com.robertmtz.propiedades.modelo.SistemaOperativo;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author UPTAP-Developer
 */
public class Csv {

    private static String nombre;
    private static SistemaOperativo sistemaOperativo;

    public Csv(String nombre, SistemaOperativo sistemaOperativo) {
        this.nombre = nombre;
        this.sistemaOperativo = sistemaOperativo;
    }

    public void exportar() {
        String salidaArchivo = nombre + ".csv"; // Nombre del archivo
        String file = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "csv" + File.separator + salidaArchivo;
        boolean existe = new File(file).exists(); // Verifica si existe

        // Si existe un archivo llamado asi lo borra
        if (existe) {
            File archivoUsuarios = new File(file);
            archivoUsuarios.delete();
        }

        try {
            // Crea el archivo
            CsvWriter salidaCSV = new CsvWriter(new FileWriter(file, true), ',');

            // Datos para identificar las columnas
            salidaCSV.write("ID");
            salidaCSV.write("Nombre");
            salidaCSV.write("Valor");
            salidaCSV.write("Sistema Operativo");

            salidaCSV.endRecord(); // Deja de escribir en el archivo

            // Recorremos la lista y lo insertamos en el archivo
            int i = 1;
            for (String s : System.getProperties().stringPropertyNames()) {
                salidaCSV.write(String.valueOf(i));
                salidaCSV.write(s);
                salidaCSV.write(System.getProperty(s));
                salidaCSV.write(sistemaOperativo.toString());

                salidaCSV.endRecord(); // Deja de escribir en el archivo}
                i++;
            }

            salidaCSV.close(); // Cierra el archivo

        } catch (IOException e) {
            System.out.println("com.robertmtz.propiedades.controlador.impl.Windows.exportar()  " + e);
        }

    }

}
