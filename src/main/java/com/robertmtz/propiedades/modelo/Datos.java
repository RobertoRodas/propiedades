/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.robertmtz.propiedades.modelo;

/**
 *
 * @author UPTAP-Developer
 */
public class Datos {

    private int id;
    private String llave;
    private String valor;

    private SistemaOperativo sistemaOperativo;

    public Datos() {
    }

    public Datos(int id, String llave, String valor, SistemaOperativo sistemaOperativo) {
        this.id = id;
        this.llave = llave;
        this.valor = valor;
        this.sistemaOperativo = sistemaOperativo;
    }

    public SistemaOperativo getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(SistemaOperativo sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Datos other = (Datos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Datos{" + "id=" + id + ", llave=" + llave + ", valor=" + valor + ", sistemaOperativo=" + sistemaOperativo + '}';
    }

}
