/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.robertmtz.propiedades.modelo;

/**
 *
 * @author UPTAP-Developer
 */
public class SistemaOperativo {

    private int id;
    private String so;

    public SistemaOperativo() {
    }

    public SistemaOperativo(int id, String so) {
        this.id = id;
        this.so = so;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SistemaOperativo other = (SistemaOperativo) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SistemaOperativo{" + "id=" + id + ", so=" + so + '}';
    }

}
